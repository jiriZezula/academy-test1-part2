{
    "id": "224253905",
    "name": "academy-1-jiri-zezula",
    "description": "",
    "created": "2020-01-04T13:11:00+0100",
    "creatorToken": {
        "id": 84511,
        "description": "jiri.zezula@gopay.cz"
    },
    "version": 8,
    "changeDescription": "Update output table out.c-11-asignement.output",
    "isDeleted": false,
    "configuration": {
        "storage": {
            "input": {
                "tables": [
                    {
                        "source": "in.c-component-creation.input",
                        "destination": "input.csv",
                        "where_column": "",
                        "where_values": [],
                        "where_operator": "eq",
                        "columns": []
                    }
                ]
            },
            "output": {
                "tables": [
                    {
                        "destination": "in.c-11-asignement.output",
                        "source": "output.csv"
                    }
                ]
            }
        },
        "parameters": {
            "print_rows": true
        }
    },
    "rowsSortOrder": [],
    "rows": [],
    "state": {
        "component": [],
        "storage": {
            "input": {
                "tables": [
                    {
                        "source": "in.c-component-creation.input",
                        "lastImportDate": "2020-09-04T09:38:03+0200"
                    }
                ]
            }
        }
    },
    "currentVersion": {
        "created": "2020-12-29T14:21:42+0100",
        "creatorToken": {
            "id": 84511,
            "description": "jiri.zezula@gopay.cz"
        },
        "changeDescription": "Update output table out.c-11-asignement.output"
    }
}